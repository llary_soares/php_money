<?php
session_start();
if (!isset($_SESSION['logado'])) {
	echo"<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
	     <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	      <meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
	      <link rel='stylesheet' href='uikit/css/uikit.min.css' />
	      <link rel='stylesheet' href='uikit/css/css.css' />
	<title>Money</title>
	</head>
	<style>
	html{
		background-color: #F5F5DC;
	}
	</style>
	<body id='menu'>
		<div>
		<div class='uk-child-width-1-1@s uk-flex uk-child-width-1-1@m uk-position-center'>
			<div  id='lugar'>
			    <div>
		            <h1 class='uk-heading-bullet'>Login</h1>
					<form action='backlogin.php' method='post'>
		            	<div class='uk-align-left'>
			                <div class='uk-margin'>
			                    <label>Nome:</label>
			                    <div class='uk-inline'>
			                        <span class='uk-form-icon' uk-icon='icon: user'></span>
			                      	<input name='user' id='user' class='uk-input' type='text' required>
			                    </div><br>
			                    <div class='uk-margin'>
				                    <label>Senha:</label>
				                    <div class='uk-inline'>
				                        <span class='uk-form-icon' uk-icon='icon: lock'></span>
				                        <input  name='senha' id='senha' class='uk-input' type='password' required>
				                    </div>
				                </div>
			                </div>
			                <center>
		                    	<button class='uk-button uk-button-default'>Logar</button>
		                    </center>
		                </div>
		                <div class='uk-align-center'>
		                	<center>Ainda não tem seu cadastro? <a href='#' id='cadastro'><br>Cadastre-se já!</a></center>
		                </div>
		            </form>
		        </div>
		    </div>
		</div>
		</div>
	<script src='uikit/js/uikit.min.js'></script>
	<script src='uikit/js/uikit-icons.min.js'></script>
	<script src='js/jquery.js'></script>
	<script src='js/ajax2.js'></script>
	</body>
	</html>";

}else{
	header("Location: home.php");
}
?>