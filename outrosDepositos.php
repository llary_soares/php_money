<?php
session_start();
if(isset($_SESSION['logado'])){
	$aux = $_SESSION['logado'];
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
	      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	      <meta http-equiv="X-UA-compatible" content="IE=edge, chrome=1"/>
	      <link rel="stylesheet" href="uikit/css/uikit.min.css" />
	      <link rel="stylesheet" href="uikit/css/css.css" />
	<title>Outros Depósitos</title>
	</head>
	<body>
		<h1 class="uk-heading-bullet">Outros Depósitos</h1>
		<?php
		echo"<form action='backsalario.php' method='post'>
			<div class='uk-margin'>
		        <label>Usuário:</label><br>
		        <div class='uk-inline'>
		            <input name='user' id='user' type='text' value='{$aux}' class='uk-input' required>
		        </div><br>
		    </div>";
		?>
			<div class='uk-margin'>
			    <label>Valor:</label><br>
			    <div class='uk-inline'>
			        <input  name='salario' id='salario' class='uk-input' type='float'>
			    </div>
			</div>
			<div class="uk-margin">
	            <button class='uk-button uk-button-default'>Registrar</button>
	        </div>
		</form>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>
	</body>
	</html>
<?php
}else{
	header("Location: index.php");
}
?>