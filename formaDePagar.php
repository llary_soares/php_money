<?php
session_start();
if (isset($_SESSION['logado'])) {
 	require_once("controle/DebitoControle.class.php");
   	require_once("modelo/Debito.class.php");
   	require_once("controle/CreditoControle.class.php");
   	require_once("modelo/Credito.class.php");
    $comando= new DebitoControle();
    $comando2 = new CreditoControle();
    $aux=$_SESSION['logado'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="IE=edge, chrome=1"/>
    <link rel="stylesheet" href="uikit/css/uikit.min.css" />
    <link rel="stylesheet" href="uikit/css/css.css" />
	<title>Money</title>
</head>
<body>
	<h1 class="uk-heading-bullet">Escolha o cartão usado para efetuar a compra</h1>
	<table class="uk-table uk-table-striped">
		<thead>
			<tr>
			    <th class="uk-text-center">Número do cartão de crédito</th>
			    <th class="uk-text-center">Selecionar</th>
			</tr>
		</thead>
		<tbody>
			 <?php
			    foreach ($comando2->selecionarCredito($_SESSION['logado']) as $conteudo) {
					echo "<tr>
						<td>{$conteudo->getNumero()} </td>
						<td><a href='compras.php? var={$conteudo->getNumero()}'>Selecionar</a></td>
					</tr>";
				}
			?>
		</tbody>
	</table>
	<table class="uk-table uk-table-striped">
		<thead>
			<tr>
			    <th class="uk-text-center">Número do cartão de débito</th>
			    <th class="uk-text-center">Selecionar</th>
			</tr>
		</thead>
		<tbody>
			<?php
			    foreach ($comando->selecionarDebito($_SESSION['logado']) as $conteudo) {
					echo "<tr>
						<td>{$conteudo->getNumero()} </td>
						<td><a href='cadastroCompras.php? var={$conteudo->getNumero()}'>Selecionar</a></td>
					</tr>";
				}
			?>
		</tbody>
	</table><br>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>
</body>
</html>
<?php
}else{
	header("Location: index.php");
}
?>