<?php
session_start();
if (isset($_SESSION['logado'])) {
	$aux = $_SESSION['logado'];
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
	      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	      <meta http-equiv="X-UA-compatible" content="IE=edge, chrome=1"/>
	      <link rel="stylesheet" href="uikit/css/uikit.min.css" />
	      <link rel="stylesheet" href="uikit/css/css.css" />
	<title>Money</title>
	</head>
	<body>
		<div class="uk-child-width-1-1@s uk-flex uk-child-width-1-2@m" uk-grid="masonry: true" style="margin-left: 30%;">
		    <div>
		    	<br>
	            <h1 class="uk-heading-bullet">Cadastrar Cartão de Débito</h1>
	            <div style="margin-left: 25%;">
		            <?php
		            echo"<form action='adddebito.php' method='post'>";
		            ?>
		            	<div class="uk-align-left">
			                <div class='uk-margin'>
			                    <label>Usuário:</label><br>
			                    <div class='uk-inline'>
			                    	<?php
			                      	echo"<input name='user' id='user' type='text' value='{$aux}' class='uk-input' required><br>";
			                      	?>
			                    </div>
			                    <div class='uk-margin'>
				                    <label>Número do cartão:</label><br>
				                    <div class='uk-inline'>
				                        <input  name='numero' id='numero' class='uk-input' type='text' required>
				                    </div>
				                </div>
			                <div class="uk-margin">
					            <button class='uk-button uk-button-default'>Registrar</button>
					        </div>
		                </div>
		            </form>
		        </div>
	        </div>
	    </div>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>
	</body>
	</html>
<?php
}else{
	header("Location: index.php");
}
?>