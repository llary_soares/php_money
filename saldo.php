<?php
session_start();
if (isset($_SESSION['logado'])) {
	echo"<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
	     <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	      <meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
	      <link rel='stylesheet' href='uikit/css/uikit.min.css' />
	      <link rel='stylesheet' href='uikit/css/css.css' />
	<title>Money</title>
	</head>
	<body id='menu' style='margin: 50px;'>
		<a href='#modal-sections' uk-toggle>Consultar Saldo</a>
            <div id='modal-sections' uk-modal>
                <div class='uk-modal-dialog'>
                    <button class='uk-modal-close-default' type='button' uk-close></button>
                    <div class='uk-modal-header'>
                        <h2 class='uk-modal-title'>Seu Saldo</h2>
                    </div>
                     <div class='uk-modal-body'>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class='uk-modal-footer uk-text-right'>
                        <button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    </div>
                 </div>
            </div>
    <script src='uikit/js/uikit.min.js'></script>
    <script src='uikit/js/uikit-icons.min.js'></script>
    <script src='js/jquery.js'></script>
    <script src='js/ajax2.js'></script>
    </body>
    </html>";

}else{
    header("Location: home.php");
}
?>