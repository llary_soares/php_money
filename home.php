<?php
session_start();
if (isset($_SESSION['logado'])) {

	echo"<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	    <meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
	    <link rel='stylesheet' href='uikit/css/uikit.min.css' />
	    <link rel='stylesheet' href='uikit/css/css2.css' />
		<title>Money</title>
	</head>
	<body style='background-color: #F5F5DC;'>
		<span class='mobile-hide'>
            <nav class='uk-navbar-container' uk-navbar class='footer' style='margin: 0px; background-color: #8B0000;'>
    		<div class='uk-navbar-center'>
        		<ul class='uk-navbar-nav'>
            		<li>
                		<a href='#'>Depósitos</a>
                		<div class='uk-navbar-dropdown' style='background-color: #F5F5DC;'>
                    		<ul class='uk-nav uk-navbar-dropdown-nav'>
		                        <li><a href='#' id='pagamentoDeSalario'>Pagamento de Salário</a></li>
		                        <li><a href='#' id='outrosDepositos'>Outros Depósitos</a></li>
		                    </ul>
                		</div>

            		</li>
            		<li>
                		<a href='#' id='saques'>Saques</a>
            		</li>
            		<li>
                		<a href='#' id='mostrarSaques'>Histórico de Saques</a>
            		</li>
            		<li>
                		<a href='#'>Cadastrar Cartão</a>
                		<div class='uk-navbar-dropdown' style='background-color: #F5F5DC'>
                    		<ul class='uk-nav uk-navbar-dropdown-nav'>
		                        <li><a href='#' id='credito'>Crédito</a></li>
		                        <li><a href='#' id='debito'>Débito</a></li>
		                    </ul>
                		</div>
            		</li>
            		<li><a href='#' id='registrarCompras'>Registrar Compras</a></li>
            		<li><a href='#' id='historicoDeCompras'>Histórico de Compras</a></li>
            		<li><a href='#modal-sections' uk-toggle>Algo para pagar?</a>
            			<div id='modal-sections' uk-modal>
                			<div class='uk-modal-dialog'>
                    			<button class='uk-modal-close-default' type='button' uk-close></button>
                   	 			<div class='uk-modal-header'>
                    			</div>
			                     <div class='uk-modal-body'>";
			                     	require_once("controle/CreditoControle.class.php");
									require_once("modelo/Credito.class.php");
									//session_start();
									$user = $_SESSION['logado'];
									$uai = new CreditoControle();
									$var2 = $uai->vencimento($user);
									echo"<span class='uk-text-large uk-text-center'>{$var2}</span>";   
                    			echo"</div>
                    			<div class='uk-modal-footer uk-text-right'>
                        			<button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    			</div>
                 			</div>
            			</div>
            		</li>
            		<li><a href='#modal-sections2' uk-toggle>Consultar Saldo</a>
            			<div id='modal-sections2' uk-modal>
                			<div class='uk-modal-dialog'>
                    			<button class='uk-modal-close-default' type='button' uk-close></button>
                    			<div class='uk-modal-header'>
                        			<h2 class='uk-modal-title'>Seu Saldo</h2>
                    			</div>
			                    <div class='uk-modal-body'>";
			                     	$aux = $_SESSION['logado'];
									require_once("modelo/Conta.class.php");
									require_once("controle/ContaControle.class.php");
									$inst = new ContaControle();
									$qualquer = $inst->total($aux);
									echo "<span class='uk-text-large uk-text-center'>Seu Saldo é de: R$ {$qualquer->getCapital()}</span>";             
                    			echo"</div>
                    		<div class='uk-modal-footer uk-text-right'>
                        		<button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    		</div>
                 		</div>
            		</li>
            		<li><a href='sair.php'>Sair</a></li>
        		</ul>
    		</div>
		</nav>
        </span>
        <span class='desktop-hide'>
        	<button class='uk-button uk-button-default' type='button' uk-toggle='target: #offcanvas-nav' style='color: #8B0000;'><span uk-icon='menu' style='color: #8B0000;'></span></button>
            <div id='offcanvas-nav' uk-offcanvas='overlay: true' style='background-color: #8B0000;'>
    			<div class='uk-offcanvas-bar' style='background-color: #8B0000;'>
        			<ul class='uk-nav uk-nav-default'>
            			<li class='uk-active'><a href='#'>Depósitos</a>
                			<ul class='uk-nav-sub'>
                    			<li><a href='#' id='pagamentoDeSalario2'>Pagamento de Salário</a></li>
                    			<li><a href='#' id='outrosDepositos2'>Outros Depósitos</a></li>
               	 			</ul>
           				</li>
            			<li class='uk-active'><a href='#' id='saques2'>Saques</a>
            			<li class='uk-active'><a href='#' id='mostrarSaques2'>Histórico de Saques</a>
            			<li class='uk-active'><a href='#'>Cadastrar Cartão</a>
                			<ul class='uk-nav-sub'>
                    			<li><a href='#' id='credito2'>Crédito</a></li>
                    			<li><a href='#' id='debito2'>Débito</a></li>
               	 			</ul>
           				</li>
           				<li class='uk-active'><a href='#' id='registrarCompras2'>Registrar Compras</a>
           				<li class='uk-active'><a href='#' id='historicoDeCompras2'>Histórico de Compras</a>
           				<li class='uk-active'><a href='#modal-sections3' uk-toggle>Algo para pagar?</a>
            			<div id='modal-sections3' uk-modal>
                			<div class='uk-modal-dialog'>
                    			<button class='uk-modal-close-default' type='button' uk-close></button>
                   	 			<div class='uk-modal-header'>
                    			</div>
			                     <div class='uk-modal-body'>";
			                     	require_once("controle/CreditoControle.class.php");
									require_once("modelo/Credito.class.php");
									//session_start();
									$user = $_SESSION['logado'];
									$uai = new CreditoControle();
									$var2 = $uai->vencimento($user);
									echo"<span class='uk-text-large uk-text-center'>{$var2}</span>";   
                    			echo"</div>
                    			<div class='uk-modal-footer uk-text-right'>
                        			<button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    			</div>
                 			</div>
            			</div>
            		</li>
            		<li class='uk-active'><a href='#modal-sections4' uk-toggle>Consultar Saldo</a>
            			<div id='modal-sections4' uk-modal>
                			<div class='uk-modal-dialog'>
                    			<button class='uk-modal-close-default' type='button' uk-close></button>
                    			<div class='uk-modal-header'>
                        			<h2 class='uk-modal-title'>Seu Saldo</h2>
                    			</div>
			                    <div class='uk-modal-body'>";
			                     	$aux = $_SESSION['logado'];
									require_once("modelo/Conta.class.php");
									require_once("controle/ContaControle.class.php");
									$inst = new ContaControle();
									$qualquer = $inst->total($aux);
									echo "<span class='uk-text-large uk-text-center'>Seu Saldo é de: R$ {$qualquer->getCapital()}</span>";             
                    			echo"</div>
                    		<div class='uk-modal-footer uk-text-right'>
                        		<button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    		</div>
                 		</div>
            		</li>
            		<li class='uk-active'><a href='sair.php'>Sair</a></li>
       	 			</ul>
    			</div>
			</div>
        </span>
		<div class='uk-text-center' id='div'  color:write;'><br><br><br>
		<img src='money.png'>
			<h1>Com esse software você pode organizar seu dinheiro e ter um maior controle das suas finanças!</h1><br><br><br><br><br><br>
		</div>
		

		<div style='background-color: #8B0000'>
			<div class='uk-grid-collapse uk-child-width-expand@s uk-text-center' uk-grid>
    			<div>
        			<div class='uk-padding'>
        				<ul class='uk-list uk-link-text'>
        					<span class='uk-text-large'>Patrocinadores</span>
        					<hr class='uk-divider-small'>
						    <li><a href='#'>Banco do Bradesco</a></li>
						    <li><a href='#'>Banco do Brasil</a></li>
						    <li><a href='#'>Caixa Econômica</a></li>
						</ul>
        			</div>
    			</div>
    			<div>
			        <div class='uk-padding'>
			        	<ul class='uk-list uk-link-text'>
        					<span class='uk-text-large'>Redes Sociais</span>
        					<hr class='uk-divider-small'>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon:facebook'></span>Facebook</a></li>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon:instagram'></span>Instagram</a></li>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon: google-plus'></span>Google Plus</a></li>
						</ul>
			        </div>
			    </div>
			    <div>
			        <div class='uk-padding'>
			        	<img src='logomoney.png' height=700px width=300px>
			        </div>
			    </div>
			</div>
		</div>
		
		<script src='uikit/js/uikit.min.js'></script>
		<script src='uikit/js/uikit-icons.min.js'></script>
		<script src='js/jquery.js'></script>
		<script src='js/ajax.js'></script>
		<script src='js/ajax3.js'></script>
		<script type='text/javascript' src='controle/script.js'></script>
	</body>
	</html>";

}else{
	header("Location: index.php");
}
?>