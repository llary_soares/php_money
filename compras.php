<?php
session_start();
if (isset($_SESSION['logado'])) {
	if(isset($_GET['var'])){
		$aux = $_SESSION['logado'];
		$variavel = $_GET['var'];
		
		echo "<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>
	    <meta http-equiv='X-UA-compatible' content='IE=edge, chrome=1'/>
	    <link rel='stylesheet' href='uikit/css/uikit.min.css' />
	    <link rel='stylesheet' href='uikit/css/css2.css' />
		<title>Money</title>
	</head>
	<body style='background-color: #F5F5DC;'>
		<nav class='uk-navbar-container' uk-navbar class='footer' style='margin: 0px; background-color: #8B0000;'>
    		<div class='uk-navbar-center'>
        		<ul class='uk-navbar-nav'>
            		<li>
                		<a href='#'>Depósitos</a>
                		<div class='uk-navbar-dropdown' style='background-color: #F5F5DC;'>
                    		<ul class='uk-nav uk-navbar-dropdown-nav'>
		                        <li><a href='home.php'>Pagamento de Salário</a></li>
		                        <li><a href='home.php'>Outros Depósitos</a></li>
		                    </ul>
                		</div>
            		</li>
            		<li>
                		<a href='home.php'>Saques</a>
            		</li>
            		<li>
                		<a href='home.php'>Histórico de Saques</a>
            		</li>
            		<li>
                		<a href='#'>Cadastrar Cartão</a>
                		<div class='uk-navbar-dropdown' style='background-color: #F5F5DC'>
                    		<ul class='uk-nav uk-navbar-dropdown-nav'>
		                        <li><a href='home.php' >Crédito</a></li>
		                        <li><a href='home.php' >Débito</a></li>
		                    </ul>
                		</div>
            		</li>
            		<li><a href='home.php'>Registrar Compras</a></li>
            		<li><a href='home.php'>Histórico de Compras</a></li>
            		<li><a href='#modal-sections' uk-toggle>Algo para pagar?</a>
            <div id='modal-sections' uk-modal>
                <div class='uk-modal-dialog'>
                    <button class='uk-modal-close-default' type='button' uk-close></button>
                    <div class='uk-modal-header'>
                    </div>
                     <div class='uk-modal-body'>";
                     	require_once("controle/CreditoControle.class.php");
						require_once("modelo/Credito.class.php");
						//session_start();
						$user = $_SESSION['logado'];
						$uai = new CreditoControle();
						$var2 = $uai->vencimento($user);
						echo"<span class='uk-text-large uk-text-center'>{$var2}</span>";
                        
                    echo"</div>
                    <div class='uk-modal-footer uk-text-right'>
                        <button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    </div>
                 </div>
            </div></li>
            		<li><a href='#modal-sections' uk-toggle>Consultar Saldo</a>
            <div id='modal-sections' uk-modal>
                <div class='uk-modal-dialog'>
                    <button class='uk-modal-close-default' type='button' uk-close></button>
                    <div class='uk-modal-header'>
                        <h2 class='uk-modal-title'>Seu Saldo</h2>
                    </div>
                     <div class='uk-modal-body'>";
                     	$aux = $_SESSION['logado'];
						require_once("modelo/Conta.class.php");
						require_once("controle/ContaControle.class.php");
						$inst = new ContaControle();
						$var = $inst->selecionarCapital($aux);
						echo "<span class='uk-text-large uk-text-center'>Seu Saldo é de: R$ {$var->getCapital()}</span>";
                        
                    echo"</div>
                    <div class='uk-modal-footer uk-text-right'>
                        <button class='uk-button uk-button-default uk-modal-close' type='button'>Ok</button>
                    </div>
                 </div>
            </div></li>
            		<li><a href='sair.php'>Sair</a></li>
        		</ul>
    		</div>
		</nav>
			<div class='uk-child-width-1-1@s uk-flex uk-child-width-1-2@m' uk-grid='masonry: true' style='margin-left: 470px;'>
			    <div>
		            <h1 class='uk-heading-bullet'>Registre sua compra</h1>
		            <form action='addcompra.php' method='post'>
		            	<div class='uk-align-left'>
			                <div class='uk-margin'>
			                    <label>Usuário:</label><br>
			                    <div class='uk-inline'>
			                      <input name='user' id='user' type='text' value='{$aux}' class='uk-input' required><br>
			                    </div>
			                    <div class='uk-margin'>
				                    <label>Produto Comprado:</label><br>
				                    <div class='uk-inline'>
				                        <input  name='produto' id='produto' class='uk-input' type='text' required>
				                    </div>
				                </div>
				                <div class='uk-margin'>
				                    <label>Valor:</label><br>
				                    <div class='uk-inline'>
				                        <input  name='valor' id='valor' class='uk-input' type='float' required>
				                    </div>
				                </div>
				                <div class='uk-margin'>
				                    <label>Data da Compra:</label><br>
				                    <div class='uk-inline'>
				                        <input  name='datacompra' id='datacompra' class='uk-input' type='date' required>
				                    </div>
				                </div>
				                <div class='uk-margin'>
				                    <label>Número do Cartão:</label><br>
				                    <div class='uk-inline'>
				                        <input name='cartao' id='cartao' type='text' value='{$variavel}' class='uk-input' required><br>
				                    </div>
				                </div>
			                </div>
			                <div class='uk-align-left'>
		                    	<button class='uk-button uk-button-default'>Cadastrar</button><br><br>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </div>
		<div style='background-color: #8B0000'>
			<div class='uk-grid-collapse uk-child-width-expand@s uk-text-center' uk-grid>
    			<div>
        			<div class='uk-padding'>
        				<ul class='uk-list uk-link-text'>
        					<span class='uk-text-large'>Patrocinadores</span>
        					<hr class='uk-divider-small'>
						    <li><a href='#'>Banco do Bradesco</a></li>
						    <li><a href='#'>Banco do Brasil</a></li>
						    <li><a href='#'>Caixa Econômica</a></li>
						</ul>
        			</div>
    			</div>
    			<div>
			        <div class='uk-padding'>
			        	<ul class='uk-list uk-link-text'>
        					<span class='uk-text-large'>Redes Sociais</span>
        					<hr class='uk-divider-small'>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon:facebook'></span>Facebook</a></li>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon:instagram'></span>Instagram</a></li>
						    <li><a href='#'><span class='uk-icon uk-margin-small-right' uk-icon='icon: google-plus'></span>Google Plus</a></li>
						</ul>
			        </div>
			    </div>
			    <div>
			        <div class='uk-padding'>
			        	<img src='logomoney.png'>
			        	<h3>Aqui seu dinheiro rende.</h3>
			        </div>
			    </div>
			</div>
		</div>
		
		<script src='uikit/js/uikit.min.js'></script>
		<script src='uikit/js/uikit-icons.min.js'></script>
		<script type='text/javascript' src='controle/script.js'></script>
	</body>
	</html>";
	}else{
		header("Location: formaDePagar.php");
	}
}else{
	header("Location: index.php");
}
?>