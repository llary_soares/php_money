<?php
session_start();
if (isset($_SESSION['logado'])) {
 	require_once("controle/ComprasControle.class.php");
   	require_once("modelo/Compras.class.php");
    $comando = new ComprasControle();
    $aux=$_SESSION['logado'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-compatible" content="IE=edge, chrome=1"/>
    <link rel="stylesheet" href="uikit/css/uikit.min.css" />
    <link rel="stylesheet" href="uikit/css/css.css" />
	<title>Money</title>
</head>
<body>
	<h1 class="uk-heading-bullet">Compras Registradas</h1><br><br>
	<table class="uk-table uk-table-striped">
    	<thead>
	        <tr>
	            <th class="uk-text-center">Produto</th>
	            <th class="uk-text-center">Valor</th>
	            <th class="uk-text-center">Data da Compra</th>
	            <th class="uk-text-center">Número do Cartão</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php
	    	foreach ($comando->selecionarTodos($_SESSION['logado']) as $conteudo) {
				echo "<tr>
					<td>{$conteudo->getProduto()} </td>
					<td>{$conteudo->getValor()} </td>
					<td>{$conteudo->getDataCompra()} </td>
					<td>{$conteudo->getNumero()} </td>
				</tr>";
			}
			?>
	    </tbody>
	</table>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>
</body>
</html>
<?php
}else{
	header("Location: index.php");
}
?>