<?php
session_start();
if (!isset($_SESSION['logado'])) {
?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	    <meta http-equiv="X-UA-compatible" content="IE=edge, chrome=1"/>
	    <link rel="stylesheet" href="uikit/css/uikit.min.css" />
	    <link rel="stylesheet" href="uikit/css/css.css" />
	<title>Money</title>
	<style>
		html{
			background-color: #F5F5DC;
		}
	</style>
	</head>
	<body>
		<div class="uk-child-width-1-1@s uk-flex uk-child-width-1-1@m uk-position-center" uk-grid="masonry: true">
		    <div>
	            <h1 class="uk-heading-bullet">Cadastre-se</h1>
	            <?php
	            echo"<form action='registrandoUser.php' method='post'>";
	            ?>
	            	<div class="uk-align-left">
		                <div class='uk-margin'>
		                    <label>Nome:</label><br>
		                    <div class='uk-inline'>
		                      	<input name='user' id='user' class='uk-input' type='text' required>
		                    </div><br>
		                    <div class='uk-margin'>
			                    <label>Senha:</label><br>
			                    <div class='uk-inline'>
			                        <input  name='pwd' id='pwd' class='uk-input' type='password' required>
			                    </div>
			                </div>
			                <div class='uk-margin'>
			                    <label>Capital inicial:</label><br>
			                    <div class='uk-inline'>
			                        <input  name='capital' id='capital' class='uk-input' type='float' required>
			                    </div>
			                </div>
		                </div>
		                <div class="uk-align-left">
	                    	<button class='uk-button uk-button-default'>Cadastrar</button><br><br>
	                    	<a href="index.php">Faça seu login!</a>
	                    </div>
	                </div>
	            </form>
	        </div>
	    </div>
	<script src="uikit/js/uikit.min.js"></script>
	<script src="uikit/js/uikit-icons.min.js"></script>
	</body>
	</html>
<?php
}else{
	header("Location: home.php");
}
?>
